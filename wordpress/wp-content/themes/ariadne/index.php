<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="format-detection" content="telephone=no">
  <meta name="Description" content="description">
  <meta name="Keywords" content="keywords">
  <title>Title</title>
  <meta property="og:title" content="Title">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta property="og:type" content="website">
  <link rel="stylesheet" href="<?php bloginfo('template_url')?>/assets/css/normalize.css">
  <link href="<?php bloginfo('template_url')?>/assets/css/et-lite.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php bloginfo('template_url')?>/assets/css/reset.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url')?>/assets/css/style.css">
  <?php wp_head()?>
</head>

<body>
  <div id="wrapper" class="wrapper">
    <header id="header" class="header">
      <div class="main_header row">
        <h1><a href="/"><img src="<?php bloginfo('template_url')?>/assets/img/logo.png" alt="ariadne"></a></h1>
      </div>
    </header>
    <!-- end header -->
    <main>
      <section id="first_view" class="first_view">
        <div class="main_sec row">
          <div class="top">
            <div class="left">
              <h2><img src="<?php bloginfo('template_url')?>/assets/img/txt_fv_01.png" alt="お金稼ぎは、全部お任せ。"></h2>
              <h3><img src="<?php bloginfo('template_url')?>/assets/img/txt_fv_02.png" alt="もう迷わない。私にはAriadneがいる。"></h3>
              <h4><img src="<?php bloginfo('template_url')?>/assets/img/txt_fv_03.png" alt="あなたの毎日に"></h4>
              <p><img src="<?php bloginfo('template_url')?>/assets/img/txt_fv_04.png" alt="月+90万円。"></p>
            </div>
            <figure><img src="<?php bloginfo('template_url')?>/assets/img/iphone.png" alt="iphone"></figure>
          </div>
          <div class="bottom">
            <div class="wrap">
              <p><img src="<?php bloginfo('template_url')?>/assets/img/txt_fv_05.png" alt="開発年数１０年。遂にタップ!!で+３万円生み出す世界初の錬金アプリが誕生!"></p>
            </div>
          </div>
        </div>
        <div id="particles-js" class="particlesjs"></div>
      </section>
      <!-- end first view -->
      <section id="step" class="step">
        <div class="step_white">
          <div class="step_gradient">
            <div class="main_sec row">
              <h2><img src="<?php bloginfo('template_url')?>/assets/img/txt_step_01.png" alt="第１話"></h2>
              <p><img src="<?php bloginfo('template_url')?>/assets/img/step_01.png" alt="step"></p>
              <p><img src="<?php bloginfo('template_url')?>/assets/img/txt_step_04.png" alt="遂に完成した世界初の錬金アプリの全貌。"></p>
              <em><img src="<?php bloginfo('template_url')?>/assets/img/txt_step_02.png" alt="何故？このシステムを使えば誰でも+90万円の利益が毎月儲かるのか？"></em>
              <span><img src="<?php bloginfo('template_url')?>/assets/img/txt_step_03.png" alt="驚愕のロジックと「アプリの正体」が遂に世界で初めて明かされる!!"></span>
            </div>
          </div>
        </div>
      </section>
      <!-- end step -->
      <div class="ytb row">
        <div class="wrap">
          <iframe id="video" width="911" height="530" src="https://www.youtube.com/embed/vDfwcN7VJKQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          <a id="play" href=""><i class="icon-video"></i></a>
        </div>
      </div>
      <!-- end ytb -->
      <div id="important" class="important">
        <div class="main_im row">
          <h2><img src="<?php bloginfo('template_url')?>/assets/img/txt_im.png" alt="最重要"></h2>
          <p><span><img src="<?php bloginfo('template_url')?>/assets/img/txt_im_01.png" alt="第１話の動画を見終えたらさっそくAriadneを無料ダウンロードしてください！"></span></p>
          <p><img src="<?php bloginfo('template_url')?>/assets/img/txt_im_02.png" alt="ご利用方法〜詳細は番組の中でお話しします。<br>まず無料ダウンロードでラインセンスを獲得してください"></p>
        </div>
      </div>
      <!-- end important -->
      <div id="system" class="system row">
        <figure><img src="<?php bloginfo('template_url')?>/assets/img/apli.png" alt="apli"></figure>
      </div>
      <!-- end system -->
      <div id="notice" class="notice">
        <div class="top">
          <p class="row"><span><img src="<?php bloginfo('template_url')?>/assets/img/txt_notice_01.png" alt="重要なお知らせ"></span></p>
        </div>
        <div class="bottom">
          <div class="main_bottom row">
            <div class="bottom_top">
              <div class="left">
                <p><img src="<?php bloginfo('template_url')?>/assets/img/txt_notice_02.png" alt="Ariadneに関する具体的な操作方法やタップで３万円(平均)の利益を得る方法を後ほどみなさんにお話をさせて頂きますので<br>まずは無料ダウンロードをしてください。"></p>
                <p><img src="<?php bloginfo('template_url')?>/assets/img/txt_notice_03.png" alt="そして非常に重要ですので“必ず第１話の動画をご覧になって”<br>お待ち頂ければ幸いです。"></p>
                <p><img src="<?php bloginfo('template_url')?>/assets/img/txt_notice_04.png" alt="遅くとも数日中にはみなさんに<br>Aradneで儲ける体験をして頂きます。"></p>
              </div>
              <div class="right"><img src="<?php bloginfo('template_url')?>/assets/img/txt_notice_05.png" alt="演者写真"></div>
            </div>
            <div class="bottom_middle"><img src="<?php bloginfo('template_url')?>/assets/img/txt_notice_06.png" alt="開発期間１０年。タップをその場で<br>キャッシュに変える錬金システムを遂にみなさんにご案内します!!"></div>
            <div class="bottom_btn btn_down">
              <a href="#"><span><img src="<?php bloginfo('template_url')?>/assets/img/btn_notice.png" alt="第１話を視聴してAriadeを<br>無料ダウンロードする。"></span></a><ins class="ripple"></ins>
            </div>
          </div>
        </div>
      </div>
      <!-- end notice -->
      <section class="st_nav_movie_nice">
        <div class="bx_ttl_movie_nice">
          <div class="bx_bkg_05">
            <div class="bkg_core">
              <div class="row">
                <h2><img src="<?php bloginfo('template_url')?>/assets/img/ttl_movie_nice.png" alt="Title movie"></h2>
              </div>
            </div>
          </div>
          <!--/.bx_bkg_05-->
        </div>
        <!--/.bx_ttl_movie_nice-->
        <div class="bx_gift_episode">
          <div class="row">
            <h3 class="first_ttl_cmm"><img src="<?php bloginfo('template_url')?>/assets/img/first_ttl_cmm.png" alt="Title Comment"></h3>
            <div class="bx_free_ari">
              <div class="top_free">
                <h4><img src="<?php bloginfo('template_url')?>/assets/img/ttl_free_ari.png" alt="Title free ari"></h4>
              </div>
              <!--/.top_free-->
              <div class="bottom_free">
                <div class="bx_bkg_07">
                  <figure><img src="<?php bloginfo('template_url')?>/assets/img/avt_free.jpg" alt="avatar"></figure>
                  <div class="bt_text">
                    <img src="<?php bloginfo('template_url')?>/assets/img/text.png" alt="text">
                  </div>
                </div>
                <!--/.bx_bkg_07-->
              </div>
              <!--/.bottom_free-->
            </div>
            <!--/.bx_free_ari-->
            <div class="bx_obtaining">
              <img src="<?php bloginfo('template_url')?>/assets/img/bx_ob.png" alt="box obtaining">
            </div>
            <!--/.bx_obtaining-->
            <div class="bx_special_method">
              <h4><img src="<?php bloginfo('template_url')?>/assets/img/ttl_special.png" alt="Title Special"></h4>
              <ul class="list_special">
                <li>
                  <span><img src="<?php bloginfo('template_url')?>/assets/img/01.png" alt="01"></span>
                  <em><img src="<?php bloginfo('template_url')?>/assets/img/txt_01.png" alt="Text 01"></em>
                  <figure>
                    <img src="<?php bloginfo('template_url')?>/assets/img/icon_pl.png" alt="icon play">
                  </figure>
                </li>
                <li>
                  <span><img src="<?php bloginfo('template_url')?>/assets/img/02.png" alt="02"></span>
                  <em><img src="<?php bloginfo('template_url')?>/assets/img/txt_02.png" alt="Text 02"></em>
                  <figure>
                    <img src="<?php bloginfo('template_url')?>/assets/img/icon_pen.png" alt="icon pen">
                  </figure>
                </li>
                <li>
                  <span><img src="<?php bloginfo('template_url')?>/assets/img/03.png" alt="03"></span>
                  <em><img src="<?php bloginfo('template_url')?>/assets/img/txt_03.png" alt="Text 03"></em>
                  <figure>
                    <img src="<?php bloginfo('template_url')?>/assets/img/icon_featured.png" alt="icon featured">
                  </figure>
                </li>
              </ul>
            </div>
            <!--/.bx_special_method-->
            <div class="btn_submit_cmm"><a href="#"><span><img src="<?php bloginfo('template_url')?>/assets/img/txt_btn_sb.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
          </div>
        </div>
        <!--/.bx_gift_episode-->
      </section>
      <!--/.st_nav_movie_nice-->
      <section id="profile" class="profile">
        <h2><img src="<?php bloginfo('template_url')?>/assets/img/txt_profile_01.png" alt="初めまして「アリアドネの糸」の小田正義です"></h2>
        <div class="wrap">
          <div class="main_sec row">
            <h3><img src="<?php bloginfo('template_url')?>/assets/img/txt_profile_02.png" alt="profile"></h3>
            <figure><img src="<?php bloginfo('template_url')?>/assets/img/photo_profile.png" alt="photo"></figure>
            <div class="right">
              <h4><img src="<?php bloginfo('template_url')?>/assets/img/txt_profile_03.png" alt="小田正義(おだまさよし)"></h4>
              <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキテキストテキストテキストテキストストテキストテキストテキテキスト(ホルダー紹介文面を挿入)テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテ</p>
            </div>
          </div>
        </div>
      </section>
      <!-- end profile -->
      <section id="hattrick_ar" class="hattrick_ar">
        <div class="main_sec row">
          <div class="tap">
            <div class="intro intro_tap"><img src="<?php bloginfo('template_url')?>/assets/img/ttl_tap.png" alt=""></div>
            <div class="txt">
              <p>初めまして<br>小田正義(おだ まさよし)です。</p>
              <p>今回はエンジニアである私が<br>開発したみなさんのタップを<br>キャッシュへと換金する<br>Ariadneというシステム</p>
              <p>そして、そのアプリを皆さんへ配布させて頂き<br>使い方であったり詳細についてお伝えさせて頂く</p>
              <p>「アリアドネの糸」というプロジェクトに<br>ご興味を持って頂きありがとうございます。</p>
              <p>この「アリアドネの糸」という企画を通して<br>私はみなさんと一緒に叶えたいゴールがあります。</p>
              <p>それはみなさんに</p>
              <p>稼ぐことが出来る本物のシステムは<br>確かにこの世に存在するということ。</p>
              <p>そして、それをみなさんに体感して頂き<br>金銭的な悩みがないライフスタイルを今日から<br>手に入れていただくということです。</p>
              <p>もちろん、「すぐ稼げる」「誰でも儲かる」<br>なんていう甘い蜜のような謳い文句ばかりが溢れ<br>お金を騙し取る気しかない悪徳業者たちが</p>
              <p>このインターネットの世界に<br>溢れていることも知っています。</p>
              <p>ですから今現時点では私の言葉は<br>みなさんには届かないかもしれません。</p>
              <p>きっと「よくあるパターンの１つ」だな....と<br>その程度に 捉え疑っていらっしゃるでしょう。</p>
              <figure><img src="<?php bloginfo('template_url')?>/assets/img/photo_tap.png" alt="photo tap"></figure>
            </div>
          </div>
          <!-- end tap -->
          <div class="certification">
            <div class="intro intro_tap"><img src="<?php bloginfo('template_url')?>/assets/img/ttl_certification.png" alt=""></div>
            <figure class="photo">
              <img src="<?php bloginfo('template_url')?>/assets/img/photo_certification.png" alt="certification">
            </figure>
            <div class="txt">
              <p>「儲かる」「稼げる」と口だけで<br>適当にホラを吹くのは簡単です。</p>
              <p>ですから今回のアリアドネの糸では<br>みなさんに私の開発したアプリケーション<br>「Ariadne 」が本物であるという事を</p>
              <p>その身で体感して頂こうと思います。</p>
              <p>私を必ず信じてください、<br>などという事は言いません。</p>
              <p>みなさん自身の目に映るもの<br>耳に聞こえるもの、心で感じるもの。</p>
              <p>それを元に判断をしてください。</p>
              <p>私と、私の開発したアプリ<br>「Ariadne」が本物であるかどうかを。</p>
              <p>もし、これから私がお話しすること<br>そして、みなさんにお見せするAriadneの<br>全てをご覧になって頂いた上でみなさんが<br>「本物である」と感じたのならばぜひ</p>
              <p>その手に取って体験してみてください。</p>
              <p>手に取ったその日から、みなさんの人生は<br>今までのそれとは全く色を変え、姿を変えて</p>
              <p>今までとは想像も出来ないくらいに<br>豊かな物へと豹変することでしょう。</p>
              <p>既に１年前にAriadneを受け取った<br>クローズドモニターたちと同じように。</p>
            </div>
            <div class="pop">
              <img src="<?php bloginfo('template_url')?>/assets/img/pop_certification.png" alt="pop certification">
            </div>
            <div class="btn_back_top">
              <a href="#"><span><img src="<?php bloginfo('template_url')?>/assets/img/txt_back_top.png" alt="back top"></span></a><ins class="ripple"></ins>
            </div>
          </div>
          <!-- end certification -->
          <div class="real">
            <div class="intro intro_tap"><img src="<?php bloginfo('template_url')?>/assets/img/ttl_real.png" alt=""></div>
            <figure class="photo">
              <img src="<?php bloginfo('template_url')?>/assets/img/photo_real.png" alt="real">
            </figure>
            <div class="txt">
              <p>ある意味、自己満足なのかもしれませんが<br>自分の開発した真に稼げるシステムが実際に<br>大勢の人に使われそしてキャッシュを生み出し</p>
              <p>さらにそれによって利用者のみなさんが<br>今よりもぐんと豊かに幸せになるという事。</p>
              <p>私にとってそれ以上の幸せはありません。</p>
              <p>エンジニアという職業柄なのかもしれませんが<br>やはり自分が長年かけて開発したシステムには</p>
              <p>ちょうど自分の子供のように<br>とても愛着が湧くものです。</p>
              <p>ですから、まずはみなさん１人１人に<br>この「Ariadne」というシステムの威力</p>
              <p>その圧倒的な利益率を実感して<br>経験して頂きたいと思っています。</p>
              <p>「月100万稼ぎましょう!!」とか<br>そんなどこぞの安っぽい謳い文句を<br>並べるつもりは一切ありません。</p>
              <p>真に良いもの</p>
              <p>真に稼げるものをみなさんに知って頂き<br>そしてそれを使って豊かになって頂きたい。</p>
              <p>その大きな大志を持って</p>
              <p>Ariadne完成と共に今回のプロジェクト<br>「アリアドネの糸」を立ち上げました。</p>
              <p>ぜひ私が開発したAriadneという<br>タップをキャッシュに換金するアプリに</p>
              <p>興味があるという方は最後まで<br>お付き合い頂ければ幸いです。</p>
              <p>きっとあなたにとっても人生に残る<br>全く新しい１ページになると思います。</p>
              <p>ぜひ、今回のアリアドネの糸と同時に<br>そんな新しい未来を歩む勇気があるのなら<br>まずはAriadneを無料ダウンローして</p>
              <p>アリアドネの糸にお付き合い頂ければ幸いです。</p>
              <p>時間は一切かかりません。</p>
              <p>一歩だけ前に出せば、もう、目の前です。</p>
            </div>
          </div>
        </div>
      </section>
      <!-- end hattrick ar -->
      <div class="bx_download_ari bx_download_ari2">
        <div class="bx_bkg_02">
          <div class="row">
            <h3><img src="<?php bloginfo('template_url')?>/assets/img/ttl_dl_free2.png" alt="title download free"></h3>
            <ul>
              <li><img src="<?php bloginfo('template_url')?>/assets/img/arr_down.png" alt="Icon Down"></li>
            </ul>
            <div class="btn_down"><a href="#"><span><img src="<?php bloginfo('template_url')?>/assets/img/txt_btn_down.png" alt="Text download"></span></a><ins class="ripple"></ins></div>
          </div>
        </div>
        <!--.bx_bkg_02-->
      </div>
      <div class="gr_wait_rule forward">
        <div class="row">
          <div class="bx_wait">
            <figure>
              <img src="<?php bloginfo('template_url')?>/assets/img/txt_intro_forward.jpg" alt="box wait">
            </figure>
          </div>
          <!--/.bx_wait-->
          <div class="bx_text">
            <p>実際に今回の企画の中ではAriadne を<br>日本全国の道ゆく人にいきなり使って頂いて<br>その場で使って頂くという企画をしました。</p>
            <p>急な企画だったのでちょっと<br>ビックリされてる方が多かったですが<br>企画の趣旨を説明させて頂いて</p>
            <p>かなりの人数の方にAriadneを<br>その場で体験して頂く事に成功しました。</p>
            <p>なにやら不審がっていた人たちも<br>実際に使って見て結果を見て驚愕。</p>
            <p>「なんだこれは、、、物凄い、、、」と<br>その儲けに驚かれる方が多かったです。</p>
            <p>今回は動画の中でもその様子を<br>お届けさせて頂いていますのでぜひ<br>後でゆっくりご覧になってください。</p>
            <p>また許可は既に頂いておりますので<br>下記に一部だけその様子を掲載しますね！</p>
          </div>
          <!--/.bx_text-->
        </div>
      </div>
      <section class="st_monitor">
        <div class="bx_monitor">
          <div class="bx_bkg_01">
            <div class="row">
              <figure>
                <img src="<?php bloginfo('template_url')?>/assets/img/ly_hand_01.png" alt="Hand 01">
              </figure>
              <h2 class="ttl_monitor"><img src="<?php bloginfo('template_url')?>/assets/img/tt_monitor.png" alt="Title Monitor"></h2>
            </div>
          </div>
          <!--/.bx_bkg_01-->
        </div>
        <!--/.bx_monitor-->
        <div class="bx_ariadne">
          <div class="row">
            <h3 class="tt_ari"><img src="<?php bloginfo('template_url')?>/assets/img/tt_ari.png" alt="Title ari"></h3>
            <ul class="list_ex_ari">
              <li>
                <figure>
                  <img src="<?php bloginfo('template_url')?>/assets/img/img_sp.png" alt="Images sample 01">
                  <figcaption>
                    <img src="<?php bloginfo('template_url')?>/assets/img/fi_bx_ex.png" alt="fi bx ex">
                  </figcaption>
                </figure>
                <div class="bx_ctn">
                  <p>
                    <em>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</em>
                  </p>
                </div>
              </li>
              <li>
                <figure>
                  <img src="<?php bloginfo('template_url')?>/assets/img/img_sp.png" alt="Images sample 01">
                  <figcaption>
                    <img src="<?php bloginfo('template_url')?>/assets/img/fi_bx_ex.png" alt="fi bx ex">
                  </figcaption>
                </figure>
                <div class="bx_ctn">
                  <p>
                    <em>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</em>
                  </p>
                </div>
              </li>
              <li>
                <figure>
                  <img src="<?php bloginfo('template_url')?>/assets/img/img_sp.png" alt="Images sample 01">
                  <figcaption>
                    <img src="<?php bloginfo('template_url')?>/assets/img/fi_bx_ex.png" alt="fi bx ex">
                  </figcaption>
                </figure>
                <div class="bx_ctn">
                  <p>
                    <em>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</em>
                  </p>
                </div>
              </li>
              <li>
                <figure>
                  <img src="<?php bloginfo('template_url')?>/assets/img/img_sp.png" alt="Images sample 01">
                  <figcaption>
                    <img src="<?php bloginfo('template_url')?>/assets/img/fi_bx_ex.png" alt="fi bx ex">
                  </figcaption>
                </figure>
                <div class="bx_ctn">
                  <p>
                    <em>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</em>
                  </p>
                </div>
              </li>
            </ul>
            <!--/.list_ex_ari-->
          </div>
        </div>
        <!--/.bx_ariadne-->
        <div class="ari_start">
          <div class="row">
            <h3 class="tt_ari_start"><img src="<?php bloginfo('template_url')?>/assets/img/tt_ari_start.png" alt="title ari start"></h3>
            <ul>
              <li><img src="<?php bloginfo('template_url')?>/assets/img/img_sp2.jpg" alt="Images Sample 02"></li>
              <li><img src="<?php bloginfo('template_url')?>/assets/img/img_sp2.jpg" alt="Images Sample 02"></li>
              <li><img src="<?php bloginfo('template_url')?>/assets/img/img_sp2.jpg" alt="Images Sample 02"></li>
            </ul>
          </div>
        </div>
        <!--/.ari_start-->
        <div class="bx_download_ari">
          <div class="bx_bkg_02">
            <div class="row">
              <h3><img src="<?php bloginfo('template_url')?>/assets/img/ttl_dl_free.png" alt="title download free"></h3>
              <ul>
                <li><img src="<?php bloginfo('template_url')?>/assets/img/arr_down.png" alt="Icon Down"></li>
                <li><img src="<?php bloginfo('template_url')?>/assets/img/arr_down.png" alt="Icon Down"></li>
                <li><img src="<?php bloginfo('template_url')?>/assets/img/arr_down.png" alt="Icon Down"></li>
              </ul>
              <div class="btn_down"><a href="#"><span><img src="<?php bloginfo('template_url')?>/assets/img/txt_btn_down.png" alt="Text download"></span></a><ins class="ripple"></ins></div>
            </div>
          </div>
          <!--.bx_bkg_02-->
        </div>
        <!--/.bx_download_ari-->
        <div class="gr_wait_rule">
          <div class="row">
            <div class="bx_wait">
              <figure>
                <img src="<?php bloginfo('template_url')?>/assets/img/bx_wait.jpg" alt="box wait">
              </figure>
            </div>
            <!--/.bx_wait-->
            <figure class="tbl_rule">
              <img src="<?php bloginfo('template_url')?>/assets/img/tbl_rule.jpg" alt="Rule">
            </figure>
            <!--/.tbl_rule-->
            <div class="bx_use_ari">
              <figure>
                <img src="<?php bloginfo('template_url')?>/assets/img/suvhead2.jpg" alt="">
              </figure>
            </div>
            <!--/.bx_use_ari-->
            <div class="bx_text">
              <p>ここまでページをご覧頂けたのならば<br>きっと私の開発したアプリケーション</p>
              <p>タップを利益に換金するAriadneに大きな<br>興味関心を寄せて頂けている事と思います。</p>
              <p>その興味関心と、行動力さえあれば大丈夫。<br>あとは全て私が丁寧にご案内させて頂きます。</p>
              <p>「あなたの人生を変える！俺に付いて来い！」なんて<br>カッコいい事は言えませんがAriadneを手に取って頂ければ<br>１００%あなたの収入を圧倒的に増やす自信があります。</p>
              <p>次回の動画では更に具体的な部分<br>すなわちAriadneの使い方に関する<br>もっと根本的な部分をお話しします。</p>
              <p>私の想いに賛同して頂き<br>最後までご覧頂いた方には等しく<br>Ariadneをお渡ししますのでぜひ<br>最後までお付き合いください。</p>
              <p>さあ、開発年数１０年。</p>
              <p>極限まで操作を簡略化してタップを<br>キャッシュに換金する事が出来るアプリ<br>「Ariadne」を世の中に伝え広め</p>
              <p>威力を日本全国の皆さんに体感して頂き<br>そして、幸せな毎日を必ず実現する</p>
              <p>プロジェクト「アリアドネの糸」の幕開けです!!</p>
              <div class="bx_who_video">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/assets/img/bx_who_video.png" alt="who video">
                </figure>
              </div>
              <!--/.bx_who_video-->
            </div>
            <!--/.bx_text-->
            <div class="btn_back_top" id="pagetop"><a href="#"><span><img src="<?php bloginfo('template_url')?>/assets/img/txt_back_top.png" alt="back top"></span></a><ins class="ripple"></ins></div>
          </div>
        </div>
        <!--/.gr_wait_rule-->
      </section>
      <!--/.st_monitor-->
      <section class="st_nav_movie_nice">
        <div class="bx_ttl_movie_nice">
          <div class="bx_bkg_05">
            <div class="bkg_core">
              <div class="row">
                <h2><img src="<?php bloginfo('template_url')?>/assets/img/ttl_movie_nice.png" alt="Title movie"></h2>
              </div>
            </div>
          </div>
          <!--/.bx_bkg_05-->
        </div>
        <!--/.bx_ttl_movie_nice-->
        <div class="bx_gift_episode">
          <div class="row">
            <h3 class="first_ttl_cmm"><img src="<?php bloginfo('template_url')?>/assets/img/first_ttl_cmm.png" alt="Title Comment"></h3>
            <div class="bx_free_ari">
              <div class="top_free">
                <h4><img src="<?php bloginfo('template_url')?>/assets/img/ttl_free_ari.png" alt="Title free ari"></h4>
              </div>
              <!--/.top_free-->
              <div class="bottom_free">
                <div class="bx_bkg_07">
                  <figure><img src="<?php bloginfo('template_url')?>/assets/img/avt_free.jpg" alt="avatar"></figure>
                  <div class="bt_text">
                    <img src="<?php bloginfo('template_url')?>/assets/img/text.png" alt="text">
                  </div>
                </div>
                <!--/.bx_bkg_07-->
              </div>
              <!--/.bottom_free-->
            </div>
            <!--/.bx_free_ari-->
            <div class="bx_obtaining">
              <img src="<?php bloginfo('template_url')?>/assets/img/bx_ob.png" alt="box obtaining">
            </div>
            <!--/.bx_obtaining-->
            <div class="bx_special_method">
              <h4><img src="<?php bloginfo('template_url')?>/assets/img/ttl_special.png" alt="Title Special"></h4>
              <ul class="list_special">
                <li>
                  <span><img src="<?php bloginfo('template_url')?>/assets/img/01.png" alt="01"></span>
                  <em><img src="<?php bloginfo('template_url')?>/assets/img/txt_01.png" alt="Text 01"></em>
                  <figure>
                    <img src="<?php bloginfo('template_url')?>/assets/img/icon_pl.png" alt="icon play">
                  </figure>
                </li>
                <li>
                  <span><img src="<?php bloginfo('template_url')?>/assets/img/02.png" alt="02"></span>
                  <em><img src="<?php bloginfo('template_url')?>/assets/img/txt_02.png" alt="Text 02"></em>
                  <figure>
                    <img src="<?php bloginfo('template_url')?>/assets/img/icon_pen.png" alt="icon pen">
                  </figure>
                </li>
                <li>
                  <span><img src="<?php bloginfo('template_url')?>/assets/img/03.png" alt="03"></span>
                  <em><img src="<?php bloginfo('template_url')?>/assets/img/txt_03.png" alt="Text 03"></em>
                  <figure>
                    <img src="<?php bloginfo('template_url')?>/assets/img/icon_featured.png" alt="icon featured">
                  </figure>
                </li>
              </ul>
            </div>
            <!--/.bx_special_method-->
            <div class="btn_submit_cmm"><a href="#"><span><img src="<?php bloginfo('template_url')?>/assets/img/txt_btn_sb.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
          </div>
        </div>
        <!--/.bx_gift_episode-->
      </section>
      <!--/.st_nav_movie_nice-->
      <section class="st_comment">
        <div class="row">
          <div class="bx_answer">
            <div class="bkg_06">
              <h2><span><img src="<?php bloginfo('template_url')?>/assets/img/txt_answer.png" alt="Text answer"></span></h2>
            </div>
          </div>
          <!--/.bx_answer-->
          <form class="frmComment" id="idFrmComment">
            <div class="frm_group">
              <span class="ans"><ins>質問1</ins></span>
              <label>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</label>
              <textarea class="frm_textarea" id="cm1"></textarea>
            </div>
            <div class="frm_group">
              <span class="ans"><ins>質問2</ins></span>
              <label>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</label>
              <textarea class="frm_textarea" id="cm2"></textarea>
            </div>
            <div class="note" id="note"></div>
            <div class="btn_submit">
              <button type="submit"><span><span><img src="<?php bloginfo('template_url')?>/assets/img/txt_btn_sb_02.png" alt="txt sm"></span></span>
              </button>
              <ins class="ripple"></ins>
            </div>
          </form>
          <!--/.frmComment-->
        </div>
      </section>
      <!--/.st_comment-->
    </main>
    <!-- end main -->
    <footer id="footer" class="footer">
      <!--#include virtual="./footer.inc"-->
    </footer>
    <!-- end footer -->
  </div>
  <!-- end wrapper -->
  <script src="<?php bloginfo('template_url')?>/assets/js/libs.js"></script>
  <script src="<?php bloginfo('template_url')?>/assets/js/reponsive_watcher.js"></script>
  <script src="<?php bloginfo('template_url')?>/assets/js/particles.js"></script>
  <script src="<?php bloginfo('template_url')?>/assets/js/app.js"></script>
  <script src="<?php bloginfo('template_url')?>/assets/js/script.js"></script>
</body>

</html>