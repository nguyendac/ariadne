<?php 
require_once("meta-box-class/my-meta-box-class.php");
// post type cm 
function cm_arr(){
  $args = array(
    'public'=>true,
    'labels' => array(
      'name'=> 'Extra Comment',
      'singular_name'=> 'Extra Comment',
      'add_new_item'=> 'New Comment'
    ),
    'supports' => array(
      'title',
    ),
  );
  register_post_type('cm', $args);
}
add_action('init', 'cm_arr');
$config_cm = array(
  'id' => 'ecm',
  'title' => 'Extra Comment',
  'pages' => array('cm'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$cm_meta = new AT_Meta_Box($config_cm);
$cm_meta->addTextarea('cm1',array('name'=> 'Comment 1'));
$cm_meta->addTextarea('cm2',array('name'=> 'Comment 2'));
$cm_meta->Finish();
// end
// include custom jQuery
function theme_scripts() {
  wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'theme_scripts');
add_action('wp_head', 'submit_form');
function submit_form(){
  ?>
  <script>
    jQuery(document).ready(function ($) {
      $(document).on('submit', '#idFrmComment', function (e) {
        e.preventDefault();
        var cm1 = $('#cm1').val();
        var cm2 = $('#cm2').val();
        $.ajax({
          type:'POST',
          url: '<?php echo admin_url(); ?>admin-ajax.php',
          data: {
              action: 'submitajax',
              cm1: cm1,
              cm2: cm2
          },
          success: function (data, textStatus, XMLHttpRequest) {
            if (data) {
              var mess = 'Success!!!';
              $('#note').removeClass('error').addClass('success').html(mess);
            } else {
              var mess = 'Fail!!';
              $('#note').removeClass('success').addClass('error').html(mess);
            }
          }
        })
      })
    })
  </script>
  <?php
}
function submitajax(){
  $cm1 = $_POST['cm1'];
  $cm2 = $_POST['cm2'];
  $msg = ('');
  if($cm1 && $cm2) {
    // date_default_timezone_set('Asia/Tokyo');
    $post_id = wp_insert_post(array (
      'post_type' => 'cm',
      'post_title' => 'comment'.current_time( 'mysql', $gmt = 0 ),
      'post_status' => 'pending',
    ));
    if($post_id){
      add_post_meta($post_id, 'cm1', $cm1);
      add_post_meta($post_id, 'cm2', $cm2);
    }
    $msg .= true;
  } else {
    $msg .= false;
  }
  die($msg);
}
add_action('wp_ajax_nopriv_submitajax', 'submitajax');
add_action('wp_ajax_submitajax', 'submitajax');
?>