window.requestAnimFrame = (function () {
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };
})();
window.cancelAnimFrame = (function () {
    return window.cancelAnimationFrame ||
        window.cancelRequestAnimationFrame ||
        window.webkitCancelAnimationFrame ||
        window.webkitCancelRequestAnimationFrame ||
        window.mozCancelAnimationFrame ||
        window.mozCancelRequestAnimationFrame ||
        window.msCancelAnimationFrame ||
        window.msCancelRequestAnimationFrame ||
        window.oCancelAnimationFrame ||
        window.oCancelRequestAnimationFrame ||
        function (id) { window.clearTimeout(id); };
})();
function closest(el,selector) {
// type el -> Object
// type select -> String
var matchesFn;
  // find vendor prefix
  ['matches','webkitMatchesSelector','mozMatchesSelector','msMatchesSelector','oMatchesSelector'].some(function(fn) {
    if (typeof document.body[fn] == 'function') {
      matchesFn = fn;
      return true;
    }
    return false;
  })
  var parent;
  // traverse parents
  while (el) {
    parent = el.parentElement;
    if (parent && parent[matchesFn](selector)) {
      return parent;
    }
    el = parent;
  }
  return null;
}
function getCssProperty(elem, property){
   return window.getComputedStyle(elem,null).getPropertyValue(property);
}
var easingEquations = {
  easeOutSine: function (pos) {
      return Math.sin(pos * (Math.PI / 2));
  },
  easeInOutSine: function (pos) {
      return (-0.5 * (Math.cos(Math.PI * pos) - 1));
  },
  easeInOutQuint: function (pos) {
      if ((pos /= 0.5) < 1) {
          return 0.5 * Math.pow(pos, 5);
      }
      return 0.5 * (Math.pow((pos - 2), 5) + 2);
  }
};
function isPartiallyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;
  return ((top + height >= 0) && (height + window.innerHeight >= bottom));
}
function isFullyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  return ((top >= 0) && (bottom <= window.innerHeight));
}
window.addEventListener('DOMContentLoaded',function(){
  new Anchor();
})
var Anchor = (function(){
  function Anchor(){
    var a = this;
    this._target = '.anchor';
    this._header = document.getElementById('header');
    this.timer;
    this.flag_start = false;
    this.iteration;
    this.eles = document.querySelectorAll(this._target);
    this.stopEverything = function(){
      a.flag_start = false;
    }
    this._getbuffer = function() {
      var _buffer;
      _buffer = a._header.clientHeight;
      return _buffer;
    }
    this._buffer = 0;
    this.scrollToY = function(scrollTargetY,speed,easing){
      var scrollY = window.scrollY || window.pageYOffset,
        scrollTargetY = scrollTargetY || 0,
        speed = speed || 2000,
        easing = easing || 'easeOutSine',
        currentTime = 0;
      var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));
      function tick() {
        if(a.flag_start){
          currentTime += 1 / 60;
          var p = currentTime / time;
          var t = easingEquations[easing](p);
          if (p < 1) {
            requestAnimFrame(tick);
            window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
          } else {
            window.scrollTo(0, scrollTargetY);
          }
        }
      }
      tick();
    }
    Array.prototype.forEach.call(this.eles,function(el,i){
      el.addEventListener('click',function(e){
        var next = el.getAttribute('href').split('#')[1];
        if(document.getElementById(next)){
          a.flag_start = true;
          e.preventDefault();
          a.scrollToY((document.getElementById(next).offsetTop-a._buffer),1500,'easeOutSine');
          if(window.innerWidth < 769) {
            document.getElementById('icon_nav').click();
          }
        }
      })
    });
    this._start = function(){
      var next = window.location.hash.split('#')[1];
      a.flag_start = true;
      if(next){
        a.scrollToY((document.getElementById(next).offsetTop - a._buffer),1500,'easeOutSine');
      }
    }
    window.addEventListener('load',a._start,false);
    document.querySelector("body").addEventListener('mousewheel',a.stopEverything,false);
    document.querySelector("body").addEventListener('DOMMouseScroll',a.stopEverything,false);
  }
  return Anchor;
})()
//Event video
$("#play").click(function(event) {
  $(this).parent().addClass("hidden");
  $("#video")[0].src += "?autoplay=1&loop=1&rel=0&wmode=transparent";
  event.preventDefault();
})
//Event video
$("#play_02").click(function(event) {
  $(this).parent().addClass("hidden");
  $("#video_02")[0].src += "?autoplay=1&loop=1&rel=0&wmode=transparent";
  event.preventDefault();
})