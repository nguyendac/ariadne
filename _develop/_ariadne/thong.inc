<section class="hattrick_ar">
  <div class="main_sec row">
    <div class="certification">
      <div class="intro intro_tap"><img src="assets/img/txt_intro_t_forward_01.jpg" alt="box wait"></div>
      <figure class="photo">
        <img src="./assets/img/photo_t_certification_01.png" alt="certification">
      </figure>
      <div class="txt">
        <p>私の人生をかけて作った物だからこそ<br>このAriadneという利益獲得アプリケーションと<br>そのユーザーの皆様のために今後の人生を費やし<br>濃密なサポートを提供する事を決めました。</p>
        <p><strong>Ariadneの大幅なアップグレードや<br>改良は行なっていきますがこれ以上のソフトは作らず<br>Ariadneのみの利益率を上げ続けると。</strong></p>
        <p><strong>そしてAriadneユーザーの皆さんにだけ<br>徹底的なサポートを提供して本当に人生が<br>圧倒的に好転するほどに稼いで頂くと。</strong></p>
        <p>そう、決心しました。</p>
        <p class="c_red fz-lagre"><strong>心から期待して貰って大丈夫です。<br>私は絶対にあなたの期待を裏切りません。</strong></p>
        <p>平均して+３万円という利益を<br>コンスタントに上げ続けるAriadneで<br>必ずあなたの人生を激変させます。</p>
        <p>そして私のアリアドネオフィシャル<br>メンバーズの一員としてこれからあなたには<br>金銭的に何一つ一切の不自由なく</p>\
        <p class="c_red fz-lagre"><strong>あなた自身が求める人生の実現を<br>私が今、この場で約束させて頂きます。</strong></p>
        <p>あなたの人生の全てを変えましょう！<br>まさに今、この場所で。</p>
      </div>
    </div> 
    <!-- end certification -->
  </div>
  <h3 class="ttl_mb"><img src="./assets/img/ttl_t_mb.png" alt="Title"></h3>
  <div class="main_sec row">
    <div class="certification">
      <div class="intro intro_tap"><img src="assets/img/txt_intro_t_forward_02.jpg" alt="box wait"></div>
      <div class="txt">
        <p>明日の１９時ちょうどに<br>アリアドネオフィシャルメンバーズ<br>募集の号令をさせて頂きます。</p>
        <p>あなたにAriadneの永久使用の<br>ライセンスを付与する日です。</p>
        <p><strong>そしてあなたがAriadneを実際に使って<br>人生を大きく激変させる日でもあります!!</strong></p>
        <p class="c_red fz-lagre"><strong>そして明日の１９時から２３時５９分までを<br>先行受付期間とし、</strong></p>
        <p class="c_red fz-lagre"><strong>その間に参加して頂いた方には<br>特別に私から豪華なプレゼントを<br>多数用意させていただきました。</strong></p>
        <p>募集開始と同時に即行動してくれた方は<br>私とAriadneことを全力で信頼してくれている<br>ということの現れだと思いますし</p>
        <p>Ariadneを使って平均+３万円という<br>キャッシュを手に入れて人生を変えたいという<br>その想いが強い方だと考えています。</p>
        <p>ですから明日の２０時から２３時５９分までに<br>参加された方には私から感謝の気持ちを込めて<br>特別な恩返しとなる特典をご用意させて頂きました。</p>
        <p>今から、その内容を発表させて頂きます！</p>
      </div>
    </div> 
    <!-- end certification -->
  </div>
</section>
<!--/.hattrick_ar-->
<section class="st_ari_mb">
  <div class="bx_ttl_ari_mb">
    <h2><img src="./assets/img/present_title.png" alt="present_title"></h2>
  </div>
  <div class="gr_special row">
    <article>
      <h3 class="ttl_art_special">
        <span><img src="./assets/img/sp_special_01.png" alt="sp_special_01"></span>
        <img src="./assets/img/ttl_special_01.png" alt="Title special 01">
      </h3>
      <div class="main_art">
        <div class="bkg_white">
          <figure>
            <img src="./assets/img/img_special_01.jpg" alt="Images Special 01">
          </figure>
          <div class="art_text">
            <p>あなたが<br>アリアドネ オフィシャルメンバーズに入会すれば、<br>何度も言うように、</p>
            <p>通常のAriadneを使うだけでも<br>「1日平均+3万円」の収入を<br>確実に得ることができます。</p>
            <p>さらに「1日平均+3万円」以上の収入を<br>得る方も続々と出てくることでしょう。</p>
            <p>ですが、これだけ詐欺が<br>横行している世の中であれば、</p>
            <p>・多額の借金がありとてもじゃないけど生活が追いつかない<br>・家族を養うには1日3万円では足りない<br>・1日でも早く貧困から脱して幸せな人生を送りたい</p>
            <p>このような悩みを抱えている人も<br>もしかしたらたくさんいるかもしれない。</p>
            <p>「1日平均+3万円」では<br>厳しいという方も大勢いるのではないだろうか？</p>
            <p>そう考えていたのです。</p>
            <p>そこで、何とか「1日平均+3万円」を<br>少しでも底上げすることはできないだろうか？</p>
            <p>と、ありとあらゆる方法を考え、<br>密かにずっと試行錯誤を繰り返して、</p>
            <p>この「Ariadne up grade ver」の<br>開発を進めていたのです。</p>
            <p>途中、何度も</p>
            <p>「もう限界か・・・」と<br>諦めそうになりましたが<br>なんとか開発することに成功しました！</p>
            <p>「Ariadne up grade ver」を使うと、<br>なんと・・・</p>
            <p>「1日平均+6万円！！」</p>
            <p>つまり、<br>1ヶ月で180万円、1年間で2,160万円を</p>
            <p class="txt_po"><span>・・</span>確実に得ることができるのです！</p>
            <p>この「Ariadne up grade ver」は、</p>
            <p>その名の通り、<br>通常のAriadneをアップグレードした、</p>
            <p>通常のAriadneの倍以上の威力を発揮する、<br>超限定シークレットアプリになります！</p>
            <p>ちなみに、面倒な作業が多い、使い方が複雑、<br>難しい設定をしなければならない<br>ということは一切ありません。</p>
            <p>通常のAriadineとやることは全く一緒です。<br>あなたが暇なときにタップをするだけです。</p>
            <p>「1日平均+6万円」を<br>生涯に渡って稼ぎ続けることができます。</p>
            <p>尚、「Ariadne up grade ver」は、<br>アリアドネ オフィシャルメンバーズの<br>先行特典限定公開となります。</p>
          </div>
        </div>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3 class="ttl_art_special">
        <span><img src="./assets/img/sp_special_02.png" alt="sp_special_02"></span>
        <img src="./assets/img/ttl_special_02.png" alt="Title special 02">
      </h3>
      <div class="main_art">
        <div class="bkg_white">
          <figure>
            <img src="./assets/img/img_special_02.png" alt="Images Special 02">
          </figure>
          <div class="art_text">
            <p>アリアドネ オフィシャルメンバーズに<br>入会したら、あなたにはもちろん<br>人生を変えて頂きます。</p>
            <p>あなたが人生を変えて幸せになったら、<br>次は、あなたが大切な人や周囲の人を<br>幸せにする番なのです。</p>
            <p>あなたは家族や大切な人を<br>幸せにする使命があるのです。</p>
            <p>そこで、あなたの子供や孫の世代まで<br>経済的不安が一切なく生きていけるように、</p>
            <p>・稼いだお金をどのように使えばいいの<br>・お金をどのように守っていけばいいのか<br>・税金などの対策はどのようにすればよいのか</p>
            <p>これらの情報を凝縮したのが<br>「マネーセオリーブック」なのです。</p>
            <p>もちろん稼いだお金で、</p>
            <p>食べたいものを食べて、<br>行きたいところに行って、<br>ほしいものを買って夢を叶えてください。</p>
            <p>ですが、それだけではなく、<br>孫の世代まで経済的不安が一切なくなるように、</p>
            <p>お金の使い方を知って、<br>稼いだお金をより有効的に使って下さい。</p>
            <p>そして、日本にいる以上は<br>どうしても稼いだお金に対して<br>税金がかかってしまうので、</p>
            <p>税金対策をしないと、<br>国に稼いだお金を持っていかれてしまいます。</p>
            <p>ですので、手元に少しでもお金が残るように<br>対策方法も載せていますので活用して<br>稼いだお金を守る、そして残してください。</p>
          </div>
        </div>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3 class="ttl_art_special">
        <span><img src="./assets/img/sp_special_03.png" alt="sp_special_03"></span>
        <img src="./assets/img/ttl_special_03.png" alt="Title special 03">
      </h3>
      <div class="main_art">
        <div class="bkg_white">
          <figure>
            <img src="./assets/img/img_special_03.jpg" alt="Images Special 03">
          </figure>
          <div class="art_text">
            <p>現在、私のLINEに登録されている方も<br>多いと思うのですが、</p>
            <p>本来であれば、現在登録されている私のLINEは<br>キャンペーン終了とともに閉鎖となります。</p>
            <p>しかし、アリアドネ オフィシャルメンバーズに入会した方には<br>私が厳選したSEや業者、投資家の方々の繋がりを生かした上で、</p>
            <p>さらには商材を私が購入したりして、<br>実際に精査し信用度が極めて高い本物の情報を<br>特別に継続して提供していくことをお約束します。</p>
            <p>私は質の高い情報や嘘の情報など、<br>常に精査していますので、<br>誰よりも早く最新の情報をあなたにお届けできます。</p>
            <p>そして私からの情報をあなたが常にチェックすることで<br>本物の情報だけを入手でき、</p>
            <p>収入はグンと上がり、<br>今後、騙されることはグンと下がる。<br>これを実現することができます。</p>
          </div>
        </div>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3 class="ttl_art_special">
        <span><img src="./assets/img/sp_special_04.png" alt="sp_special_04"></span>
        <img src="./assets/img/ttl_special_04.png" alt="Title special 04">
      </h3>
      <div class="main_art">
        <div class="bkg_white">
          <figure>
            <img src="./assets/img/img_special_04.jpg" alt="Images Special 04">
          </figure>
          <div class="art_text">
            <p>Ariadneを使って取引を始めるには<br>もちろん元手が必要になります。</p>
            <p>ですが、今お金がないという方は、<br>たくさんいらっしゃると思います。</p>
            <p>そこで、<br>「詐欺被害者救済 10万円錬金術」を使えば、</p>
            <p>元手となる10万円を<br>最も効率よく生み出すことができます。</p>
            <p>そうすることで、手持ちのお金を1円も使うことなく、<br>さらに、手持ちのお金がなくても<br>取引を始めることができるのです。</p>
          </div>
        </div>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3 class="ttl_art_special">
        <span><img src="./assets/img/sp_special_05.png" alt="sp_special_05"></span>
        <img src="./assets/img/ttl_special_05.png" alt="Title special 05">
      </h3>
      <div class="main_art">
        <div class="bkg_white">
          <figure>
            <img src="./assets/img/img_special_05.jpg" alt="Images Special 05">
          </figure>
          <div class="art_text">
            <p>Ariadneは、<br>トラブル防止のための<br>システムのメンテナンス、</p>
            <p>最新コンテンツの提供、<br>そして、使用者がより快適に、<br>より使いやすくするために、</p>
            <p>性能のグレードを向上させて、<br>最新版を常に使用者の方に<br>使ってもらうことを目的としています。</p>
            <p>そのためには定期的な<br>アップロードが必要となるのです。</p>
            <p>そのアップロードを無料で行えるのが<br>「Ariadne 無料アップグレード権利」です。</p>
            <p>常に最新のシステムに更新することで<br>最新のトレンドを逃すこともなくなるので、<br>継続的にサクッと稼ぎ続けることが可能になります。</p>
          </div>
        </div>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3 class="ttl_art_special">
        <span><img src="./assets/img/sp_special_06.png" alt="sp_special_06"></span>
        <img src="./assets/img/ttl_special_06.png" alt="Title special 06">
      </h3>
      <div class="main_art">
        <div class="bkg_white">
          <figure>
            <img src="./assets/img/img_special_06.jpg" alt="Images Special 06">
          </figure>
          <div class="art_text">
            <p>Ariadneは説明書が不要なくらい、<br>簡単に操作できる仕組みになっているのですが、</p>
            <p>万が一、わからないことなどが出てきたときに、<br>すぐ解決できる場として、<br>専用サポートチームを用意しています。</p>
            <p>わからないことがあれば、<br>専用サポートチームに連絡をして下さい。</p>
            <p>ホットラインとして<br>早急にあなたに対応してくれますので<br>ご安心ください。</p>
            <p>メールではなく、<br>直接通話をしての対応となりますので<br>わからないことがあってもすぐに解決ができます。</p>
          </div>
        </div>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3 class="ttl_art_special">
        <span><img src="./assets/img/sp_special_07.png" alt="sp_special_07"></span>
        <img src="./assets/img/ttl_special_07.png" alt="Title special 07">
      </h3>
      <div class="main_art">
        <div class="bkg_white">
          <figure>
            <img src="./assets/img/img_special_07.jpg" alt="Images Special 07">
          </figure>
          <div class="art_text">
            <p>Ariadneをあなたが一生涯使い続けるための<br>永久ライセンスを発行します。</p>
            <p>またライセンスを取得することで、<br>あなたが、後に誰かにこのアプリを紹介したい、<br>販売したいとなった時には、</p>
            <p>代理権販売の権利を審査なく<br>取得することが可能となります。</p>
            <p>こちらについては入会後に詳しくお教えします。<br>こちらについては入会後に詳しくお教えします。</p>
          </div>
        </div>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3 class="ttl_art_special">
        <span><img src="./assets/img/sp_special_08.png" alt="sp_special_08"></span>
        <img src="./assets/img/ttl_special_08.png" alt="Title special 08">
      </h3>
      <div class="main_art">
        <div class="bkg_white">
          <figure>
            <img src="./assets/img/img_special_08.png" alt="Images Special 08">
          </figure>
          <div class="art_text">
            <p>アプリと連携している<br>バイナリーオプションは、<br>取引口座を登録したりしないと<br>いけません。</p>
            <p>何も難しいことがなく、<br>初心者や素人の方でもできるのですが</p>
            <p>重点的に分かりやすい言葉で<br>非常に</p>
            <p>「Ariadne 初心者ガイドブック」</p>
            <p>これがあれば、問題なく、<br>スムーズに確実に登録できることでしょう。</p>
          </div>
        </div>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3 class="ttl_art_special">
        <span><img src="./assets/img/img_top_09.png" alt=""></span>
        <span><img src="./assets/img/sp_special_09.png" alt="sp_special_09"></span>
        <img src="./assets/img/ttl_special_09.png" alt="Title special 09">
      </h3>
      <div class="main_art">
        <div class="bkg_white">
          <figure>
            <img src="./assets/img/img_special_09.jpg" alt="Images Special 09">
          </figure>
          <div class="art_text">
            <p class="c_red"><strong>今この場ではその内容を明かせませんが<br>他の先行特典を凌駕するほどの圧倒的な<br>スペシャル特典を用意させて頂きました。</strong></p>
            <p>Ariadneで人生を変えたいと心から願う<br>あなたに少しでも心の余裕を持って頂き<br>スタート同時に大きく稼いで頂きたい。</p>
            <p>そしてアリアドネオフィシャルメンバーズに<br>参加した、まさにその日から人生を変えてほしい。</p>
            <p>Ariadneで+ ３万円というキャッシュを<br>コンスタントに手に入れ続けていただきたい。</p>
            <p>その思いでこの特典を用意しました。</p>
            <p>明日の１９時と同時に見た時には<br>きっと、ビックリすると思います。</p>
            <p>そんな物凄いスペシャルな<br>ボーナス特典をご用意しました！<br>ぜひ、強くご期待ください！</p>
          </div>
        </div>
      </div>
      <!--/.main_art-->
    </article>
  </div>
  <!--/.gr_special-->
</section>
<!--/.st_ari_mb-->