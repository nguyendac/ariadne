<section id="first_view" class="first_view">
  <div class="main_sec row">
    <div class="top">
      <div class="left">
        <h2><img src="./assets/img/txt_fv_01.png" alt="お金稼ぎは、全部お任せ。"></h2>
        <h3><img src="./assets/img/txt_fv_02.png" alt="もう迷わない。私にはAriadneがいる。"></h3>
        <h4><img src="./assets/img/txt_fv_03.png" alt="あなたの毎日に"></h4>
        <p><img src="./assets/img/txt_fv_04.png" alt="月+90万円。"></p>
      </div>
      <figure><img src="./assets/img/iphone.png" alt="iphone"></figure>
    </div>
    <div class="bottom">
      <div class="wrap">
        <p><img src="./assets/img/txt_fv_05.png" alt="開発年数１０年。遂にタップ!!で+３万円生み出す世界初の錬金アプリが誕生!"></p>
      </div>
    </div>
  </div>
  <div id="particles-js" class="particlesjs"></div>
</section>
<!-- end first view -->
<nav class="menu">
  <ul>
    <li><a href="http://ariadne-project2018.com/?id=ejidfk1&no=iec" target="_blank"><img src="./assets/img/menu1.png" alt=""></a></li>
    <li><a href="http://ariadne-project2018.com/?id=cioeao2&no=pcd" target="_blank"><img src="./assets/img/menu2.png" alt=""></a></li>
    <li><a href="http://ariadne-project2018.com/?id=miodie3&no=eix" target="_blank"><img src="./assets/img/menu3.png" alt=""></a></li>
    <li><a href="http://ariadne-project2018.com/?id=ivedex4&no=zni"><img src="./assets/img/menu4.png" alt=""></a></li>
    <li><img src="./assets/img/menu5.png" alt=""></li>
  </ul>
</nav>
<!-- LP5 -->
<section id="last_story" class="last_story">
  <div class="main_sec row">
    <h2><img src="./assets/img/last_stoty_01.png" alt=""></h2>
    <p><img src="./assets/img/last_stoty_02.png" alt=""></p>
    <p><img src="./assets/img/last_stoty_03.png" alt=""></p>
  </div>
</section>
<div class="not_seen">
  <div class="main_not_seen">
    <figure>
      <img src="./assets/img/arrow_tripper.png" alt="">
      <figcaption><img src="./assets/img/not_seen_01.png" alt=""></figcaption>
    </figure>
    <div class="bottom_btn btn_down">
      <a href="http://ariadne-project2018.com/?id=ivedex4&no=zni" target="_blank"><span><img src="./assets/img/txt_back_top03.png" alt="第１話を視聴してAriadeを<br>無料ダウンロードする。"></span></a><ins class="ripple"></ins>
    </div>
  </div>
</div>
<figure class="all_ariadne"><img src="./assets/img/all_ariadne.png" alt=""></figure>
<section id="step02" class="step02">
  <div class="step02_white">
    <div class="step02_gradient">
      <div class="main_sec row">
        <h2><img src="./assets/img/txt_step02_01.png" alt="第１話"></h2>
        <p><img src="./assets/img/txt_step02_02.png" alt="step"></p>
        <p><img src="./assets/img/txt_step02_03.png" alt="遂に完成した世界初の錬金アプリの全貌。"></p>
        <p><img src="./assets/img/txt_step02_04.png" alt="何故？このシステムを使えば誰でも+90万円の利益が毎月儲かるのか？"></p>
      </div>
    </div>
  </div>
</section>
<div class="bgytb">
  <div class="ytb row" id="ytb">
    <div class="wrap">
      <iframe id="video" width="911" height="530" src="https://www.youtube.com/embed/vDfwcN7VJKQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      <a id="play" href=""><i class="icon-video"></i></a>
    </div>
  </div>
</div>
<section class="st_nav_movie_nice">
  <div class="bx_ttl_movie_nice">
    <div class="bx_bkg_05">
      <div class="bkg_core">
        <div class="row">
          <h2><img src="./assets/img/ttl_movie_nice.png" alt="Title movie"></h2>
        </div>
      </div>
    </div>
    <!--/.bx_bkg_05-->
  </div>
  <!--/.bx_ttl_movie_nice-->
  <div class="bx_gift_episode">
    <div class="row">
      <p class="movie_extra"><img src="./assets/img/movie_nice_extra.png" alt=""></p>
      <h3 class="first_ttl_cmm"><img src="./assets/img/first_ttl_cmm.png" alt="Title Comment"></h3>
      <div class="bx_free_ari">
        <div class="top_free">
          <h4><img src="./assets/img/ttl_free_ari.png" alt="Title free ari"></h4>
        </div>
        <!--/.top_free-->
        <div class="bottom_free">
          <div class="bx_bkg_07">
            <figure><img src="./assets/img/avt_free.jpg" alt="avatar"></figure>
            <div class="bt_text">
              <img src="./assets/img/text.png" alt="text">
            </div>
          </div>
          <!--/.bx_bkg_07-->
        </div>
        <!--/.bottom_free-->
      </div>
      <!--/.bx_free_ari-->
      <div class="bx_obtaining">
        <img src="./assets/img/bx_ob.png" alt="box obtaining">
      </div>
      <!--/.bx_obtaining-->
      <div class="bx_special_method">
        <h4><img src="./assets/img/ttl_special.png" alt="Title Special"></h4>
        <ul class="list_special">
          <li>
            <span><img src="./assets/img/01.png" alt="01"></span>
            <em><img src="./assets/img/txt_01.png" alt="Text 01"></em>
            <figure>
              <img src="./assets/img/icon_pl.png" alt="icon play">
            </figure>
          </li>
          <li>
            <span><img src="./assets/img/02.png" alt="02"></span>
            <em><img src="./assets/img/txt_02.png" alt="Text 02"></em>
            <figure>
              <img src="./assets/img/icon_pen.png" alt="icon pen">
            </figure>
          </li>
          <li>
            <span><img src="./assets/img/03.png" alt="03"></span>
            <em><img src="./assets/img/txt_03.png" alt="Text 03"></em>
            <figure>
              <img src="./assets/img/icon_featured.png" alt="icon featured">
            </figure>
          </li>
        </ul>
      </div>
      <!--/.bx_special_method-->
      <div class="btn_submit_cmm"><a href="#idFrmComment" class="anchor"><span><img src="./assets/img/txt_btn_sb.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
    </div>
  </div>
  <!--/.bx_gift_episode-->
</section>
<section id="hattrick_ar" class="hattrick_ar">
  <div class="main_sec row">
    <div class="tap">
      <div class="intro intro_tap"><img src="./assets/img/ttl_tap.png?v=7d592130e5476b4969f3122a64ae23f8" alt=""></div>
      <div class="txt">
        <p>こんにちは。<br>小田です。</p>
        <p>遂に今からAriadneの全てを<br>今、私のメッセージをご覧に皆さんへ<br>ご案内させて頂く日が来ました。</p>
        <p>今まで様々なビジネスに挑戦して<br>大切なお金と時間を費やしたにも関わらず<br>どう頑張っても成功できなかった方。</p>
        <p><strong>「本当に儲かる」と囁かれて買った<br>高額ツールやシステムを購入したものの<br>全く儲からず借金ばかりが増えた方。</strong></p>
        <p><strong>「次こそは、次こそは、、、」と夢に見て<br>今の今まで本当に精一杯頑張って来たけれど<br>努力実らず、想い報われなかった方。</strong></p>
        <p class="c_red"><strong>そんな方を”救う”というと偉そうですが<br>現状の胃が痛むような危機的状況から抜け出し</strong></p>
        <p class="c_red"><strong>金銭的にも精神的にも一切の悩みや不安のない<br>「あなたが本当望む自由な未来」を実現する<br>あなたの「明確な武器」となります。</strong></p>
        <p>毎日平均して+３万円のキャッシュが<br>コンスタントに入ってくるということは<br>月にして計算してみると約９０万円。</p>
        <p>年間では９０万円×１２ヶ月で１０８０万円です。</p>
        <p>１０８０万円というは日本で言えば<br>人口の１０%に満たない医者や一流弁護士パイロットといった<br>”エリートと言われる職種”ですから</p>
        <p class="c_red"><strong>今、あなたが抱えている悩みの大半は<br>綺麗に解決する事ができる事かと思います。</strong></p>
        <p>あなたの人生を切り開く<br>その力を持つのがAriadneです。</p>
        <p>本当におめでとうございます。</p>
        <p class="s_large"><strong>まさに今日からAriadneを使って<br>全く新しい人生をスタートしましょう!!</strong></p>
        <figure><img src="./assets/img/photo_tap.png?v=f29f3c7b240b33b0edbd674a69e995ef" alt="photo tap"></figure>
        <div class="pop">
          <img src="./assets/img/pop_certification.png" alt="pop certification">
        </div>
        <div class="btn_back_top btn_extra">
          <a href="#ytb" class="anchor"><span><img src="assets/img/txt_back_top.png" alt="back top"></span></a><ins class="ripple"></ins>
        </div>
      </div>
    </div>
    <div class="real">
      <div class="txt">
        <div class="three_step">
          <div class="group_step">
            <p><img src="./assets/img/three_step_01.png" alt=""></p>
            <p><img src="./assets/img/three_step_02.png" alt=""></p>
            <p><img src="./assets/img/three_step_03.png" alt=""></p>
          </div>
          <p><img src="./assets/img/three_step_04.png" alt=""></p>
        </div>
        <div class="bx_step">
          <p><img src="assets/img/img_step.png?v=f09e80f3bd7c5e87b463ac745d16c598" alt="Step"></p>
        </div>
        <div class="bx_profit">
          <p><img src="assets/img/ttl_profit.png?v=02d286a1d1e0b6d01c0076fed390e2fd" alt="Profit"></p>
          <p><img src="assets/img/img_money.png?v=ff35f006b7365c197ed3497b7d7287b2" alt="Profit"></p>
          <p><img src="assets/img/txt_profit.png?v=40a0c3a1f56869761e560bd299285199" alt="Profit"></p>
        </div>
      </div>
    </div>
    <div class="tap">
      <div class="intro intro_tap"><img src="./assets/img/ttl_tap11.png?v=7d592130e5476b4969f3122a64ae23f8" alt=""></div>
      <div class="txt">
        <p>最近のプロモーションではICOで誰でも<br>カンタンに「億り人」や「数百倍確定！」などと言った<br>甘い蜜のような謳い文句で溢れています。</p>
        <p><strong>しかしその中でも大半は詐欺まがいの<br>プロダクトのないものばかりです。</strong></p>
        <p>そればかりかICO案件が本物であっても<br>実際に数百倍の値上がりを見せる物と言うのは<br>１００に１つの世界ではありません。</p>
        <p>万に１つの非常にシビアな世界です。</p>
        <p class="c_red"><strong>つまりはそんな天文学的確率で<br>ICOで儲けるなど不可能な話です。</strong></p>
        <p>そもそも数万円ぽっちを元本にして<br>１ヶ月、２ヶ月で誰でも億を稼げるなら<br>日本はもう大金持ちばかりでしょう。</p>
        <p>そんな簡単な話ではないのです。</p>
        <p>ですから、改めてこの場で断言しておくと<br>私の「Ariadne」は月1億円を稼ぐような<br>爆発的な利益獲得システムではありません。</p>
        <p class="c_red s_large"><strong>そうではなくてAriadeはコンスタントに<br>枯れることなく金の卵をうみニワトリのように<br>毎日数万円を生み出し続けるシステムです。</strong></p>
      </div>
    </div>
    <div class="tap">
      <div class="intro intro_tap">
        <img src="./assets/img/ttl_tap12.png?v=7d592130e5476b4969f3122a64ae23f8" alt="">
      </div>
      <div class="txt">
        <div class="wrap_img"><img src="./assets/img/tap_img01.jpg" alt=""></div>
        <p>仮に単発のギャンブルで一瞬で100万円が入ってきても<br>あなたの人生はそこまで大きく変わることはないでしょう。</p>
        <p class="c_red"><strong>でも、毎日+３万円が安定して<br>コツコツとあなたに入ってきたら？</strong></p>
        <p class="c_red"><strong>月間にして+９０〜１００万円近い<br>大きな収入がキャッシュで増えたら？</strong></p>
        <p class="c_red"><strong>それも「スマートフォンの画面をタップする」という<br>非常にシンプルな作業のみで実現可能になるとしたら？</strong></p>
        <p>それらがあなたの人生に与える<br>良いインパクトは本当に絶大です。</p>
        <p class="c_red s_large"><strong>毎日３万円という金額は非常に大きいです。</strong></p>
        <p><strong>例えがありきたりですが都内にある<br>回らない高級寿司店やグレードのかなり高い<br>神戸牛の焼き肉などを毎日食べ続けたとしても<br>たくさんお釣りがくるほどの金額です。</strong></p>
        <p>月に一度くらいは自分の家族を連れて<br>ハワイやグアムなどの南国旅行に旅行で<br>出かけても十分に満喫できますし</p>
        <p>自分自身はもちろんですがご家族など<br>あなたの大切な人に好きな時に好きなだけ<br>プレゼントを送ることだって出来ます。</p>
        <p class="c_red s_large"><strong>自分自身はもちろんですがご家族など<br>あなたの大切な人に好きな時に好きなだけ<br>プレゼントを送ることだって出来ます。</strong></p>
        <p class="c_red"><strong>そんな自由で満たされた毎日です。</strong></p>
        <p>もちろんそんな贅沢な暮らしを<br>望むか、望まないかは人それぞれですが<br>少なくともあなたが幸せな暮らしを送るに<br>十分に足る金額だと思います。</p>
        <p>そして</p>
        <p>今このメッセージをご覧のあなたなら<br>Ariadneにはその力があるという事を既に<br>しっかりと理解されて入ると思います。</p>
        <p>さて、いよいよです！！</p>
        <p>ついにAriadneをあなた自身に<br>使って頂き硬く利益を儲けて頂く<br>その時が、やってきました！</p>
      </div>
    </div>
  </div>
</section>
<section id="member" class="hattrick_ar member">
  <figure><img src="./assets/img/member.png" alt=""></figure>
  <div class="main_sec row">
    <div class="tap">
      <div class="intro intro_tap">
        <img src="./assets/img/tap_invitation.png" alt="">
      </div>
      <div class="txt">
        <p>正直な話をすると、今回の企画を経て<br>アリアドネオフィシャルメンバーズという<br>コミュニティを立ち上げるのかどうか？</p>
        <p>私自身、非常に悩みました。</p>
        <p>元より人前に立つ性格ではないですし<br>Ariadneを使って貰うだけならばこうした<br>コミュニティを作らずともアプリだけ<br>あなたにダウンロードをして頂いて</p>
        <p>ライセンス発行をすれば良いのでは？</p>
        <p><strong>Ariadne自体はタップで使えますし<br>それだけで平均+３万円のキャッシュが<br>簡単に誰でも手に入ってしまうので</strong></p>
        <p><strong>何か難しい設定や作業が必要な物でもありません。</strong></p>
        <p>一方で</p>
        <p>コミュニティを立ち上げてサポートしたり<br>関わり合いを作るとなると労力がかかります。</p>
        <p class="c_red"><strong>もしも作るとなれば私の職業柄どうしても<br>中途半端なことはせず徹底的にサポートをして</strong></p>
        <p class="c_red"><strong>運営をしなければ気が済みませんので<br>適当に雰囲気だけで作る事はできません。</strong></p>
        <p>ですから、Ariadneだけを提供させて頂くか<br>それともAriadneを１００%活用して頂くべく<br>最適な環境(コミュニティ)まで提供するか。</p>
        <p>本当に悩みました。</p>
        <figure><img src="./assets/img/oda_tr1.png" alt=""></figure>
      </div>
    </div>
  </div>
</section>
<!-- LP5 -->
