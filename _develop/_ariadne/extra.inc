<section class="hattrick_ar">
  <div class="main_sec row">
    <div class="tap">
      <div class="intro intro_tap"><img src="./assets/img/ttl_tap31.png" alt=""></div>
      <div class="txt">
        <p class="c_red"><strong>このアリアドネオフィシャルメンバーズこそが<br>あなたが「Ariadne」をフル活用して毎日平均して<br>+３万円というキャッシュを手に入れる事ができる<br>ベストな環境であることは間違いありません。</strong></p>
        <p>そして、タップをキャッシュに換金するという<br>誰でも実行可能なほどに簡単な仕組みこそが</p>
        <p>私が「誰でも利益を積み上げられる」と<br>そう断言できる確固たる理由なのです。</p>
        <p><strong>「稼ぐ」というのはもはや当然のことです。</strong></p>
        <p>あとはいかに安定的に長期で稼げるか。<br>そして一回のタップで稼げる利益金額を<br>より多くできるかにかかっています。</p>
        <p class="c_red"><strong>そして大切な所は仮想通貨の<br>バイナリーオプションであるものの</strong></p>
        <p class="c_red s_large"><strong>現金(キャッシュ)であなたの元へと<br>着金するという点も見逃せません。</strong></p>
        <p>近年では仮想通貨で１億円稼ぎました！<br>という風な人も、実は</p>
        <p>「１億円分の含み益」を持っています。<br>というパターンである事が大半です。</p>
        <p>それが「売れなければ」ただの<br>データの塊と化してしまいます。</p>
        <p>そして今、BTCやETHというメジャーな除けば<br>取引量の小さいICOされたばかりのマイナーコインを</p>
        <p>１億円はおろか、５００万円分利確することさえも</p>
      </div>
    </div>
    <!-- end tap -->
    <div class="certification">
      <div class="intro intro_tap"><img src="assets/img/ttl_tap32.png" alt="box wait"></div>
      <figure class="photo">
        <img src="./assets/img/photo_certification_02.png" alt="certification">
      </figure>
      <div class="txt">
        <p>私も仮想通貨はもちろん持ってますが<br>今の世の中はあまりにも「仮想通貨」ばかりが<br>騒がれすぎているようにも感じます。</p>
        <p>結果的に仮想通貨はたくさんあるけど<br>肝心のキャッシュが全然ない方が多いです。</p>
        <p><strong>仮想通貨はまだまだ世界で<br>通用するには不十分です。</strong></p>
        <p class="c_red"><strong>やはり現金(キャッシュ)なのです。</strong></p>
        <p>ですからAriadneでは仮想通貨ではなく<br>毎日＋３万円の現金を手に入れるという所を<br>強くイメージして制作をしてきました。</p>
        <p>私が１０年かけて作った集大成のシステム。</p>
        <p>そして</p>
        <p class="c_red"><strong>誰もがキャッシュを手軽に手に入れられる<br>世界屈指のアプリこそが「Aridne」であり</strong></p>
        <p class="c_red"><strong>それを用いて平均＋３万円のキャッシュを<br>手に入れていく上で最高の環境こそが</strong></p>
        <p class="c_red"><strong>まさに明日の１９時にその扉が開かれる<br>「アリアドネオフィシャルメンバーズ」です。</strong></p>
        <p><strong>今回の募集は私にとって<br>最初で最後となります。</strong></p>
        <p>もう二度はない<br>アリアドネオフィシャルメンバーズと<br>Ariadneの永久利用ライセンスをぜひ<br>明日の１９時に受け取ってください。</p>
        <p>もうこれ以上は絶対に失敗できない。<br>絶対に間違えたりお金を減らすことはできない。</p>
        <p>そうあなたが思うのならば<br>アリアドネオフィシャルメンバーズに<br>明日、仲間入りしてください。</p>
        <p>私はあなたの真剣な想いを<br>本当に強く、強く感じています。</p>
        <p>その証拠にあなたはこれほどに長い<br>私の強い想いのこもったメッセージを<br>丁寧に最後まで読んでくれています。</p>
        <p><strong>今、私が断言します。</strong></p>
        <p class="c_red s_large"><strong>今のあなたの生活につきまとう<br>金銭的な悩みや不安やストレスの全ては<br>明日の１９時で最後です。</strong></p>
        <p class="c_red"><strong>報われなかった想いが報われ<br>実らなかった今までの努力の全てが<br>明日の１９時に</strong></p>
        <p class="c_red"><strong>アリアドネオフィシャルメンバーズの<br>扉が開かれると同時に次々と実っていきます。</strong></p>
        <p>大丈夫です。もう安心してください。</p>
        <p>私が必ずあなたの人生を変えると、<br>一切不安や悩みのない物にすると、</p>
        <p>そう、宣言します。</p>
      </div>
    </div>
    <div class="cert">
      <figure><img src="./assets/img/cert.png" alt=""></figure>
      <p>それでは、明日の１９時。<br>人生で一番の期待を持って</p>
      <p>アリアドネオフィシャルメンバーズの<br>扉が開かれる瞬間をお待ちください!!</p>
      <p>小田 正義</p>
    </div>
  </div>
</section>
<section class="st_nav_movie_nice">
  <div class="bx_ttl_movie_nice">
    <div class="bx_bkg_05">
      <div class="bkg_core">
        <div class="row">
          <h2><img src="./assets/img/ttl_movie_nice.png" alt="Title movie"></h2>
        </div>
      </div>
    </div>
    <!--/.bx_bkg_05-->
  </div>
  <!--/.bx_ttl_movie_nice-->
  <div class="bx_gift_episode">
    <div class="row">
      <p class="movie_extra"><img src="./assets/img/movie_nice_extra.png" alt=""></p>
      <h3 class="first_ttl_cmm"><img src="./assets/img/first_ttl_cmm.png" alt="Title Comment"></h3>
      <div class="bx_free_ari">
        <div class="top_free">
          <h4><img src="./assets/img/ttl_free_ari.png" alt="Title free ari"></h4>
        </div>
        <!--/.top_free-->
        <div class="bottom_free">
          <div class="bx_bkg_07">
            <figure><img src="./assets/img/avt_free.jpg" alt="avatar"></figure>
            <div class="bt_text">
              <img src="./assets/img/text.png" alt="text">
            </div>
          </div>
          <!--/.bx_bkg_07-->
        </div>
        <!--/.bottom_free-->
      </div>
      <!--/.bx_free_ari-->
      <div class="bx_obtaining">
        <img src="./assets/img/bx_ob.png" alt="box obtaining">
      </div>
      <!--/.bx_obtaining-->
      <div class="bx_special_method">
        <h4><img src="./assets/img/ttl_special.png" alt="Title Special"></h4>
        <ul class="list_special">
          <li>
            <span><img src="./assets/img/01.png" alt="01"></span>
            <em><img src="./assets/img/txt_01.png" alt="Text 01"></em>
            <figure>
              <img src="./assets/img/icon_pl.png" alt="icon play">
            </figure>
          </li>
          <li>
            <span><img src="./assets/img/02.png" alt="02"></span>
            <em><img src="./assets/img/txt_02.png" alt="Text 02"></em>
            <figure>
              <img src="./assets/img/icon_pen.png" alt="icon pen">
            </figure>
          </li>
          <li>
            <span><img src="./assets/img/03.png" alt="03"></span>
            <em><img src="./assets/img/txt_03.png" alt="Text 03"></em>
            <figure>
              <img src="./assets/img/icon_featured.png" alt="icon featured">
            </figure>
          </li>
        </ul>
      </div>
      <!--/.bx_special_method-->
      <div class="btn_submit_cmm"><a href="#idFrmComment" class="anchor"><span><img src="./assets/img/txt_btn_sb.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
    </div>
  </div>
  <!--/.bx_gift_episode-->
</section>
<!--/.st_nav_movie_nice-->
<section class="st_comment">
  <div class="row">
    <div class="bx_answer">
      <div class="bkg_06">
        <h2><span><img src="./assets/img/txt_answer.png" alt="Text answer"></span></h2>
      </div>
    </div>
    <!--/.bx_answer-->
    <form class="frmComment" id="idFrmComment">
      <div class="frm_group">
        <span class="ans"><ins>質問1</ins></span>
        <label>アリアドネ オフィシャルメンバーズに賭ける、<br>あなたのありったけの想いを書いて下さい。</label>
        <textarea class="frm_textarea"></textarea>
      </div>
      <div class="btn_submit">
        <button type="submit"><span><span><img src="./assets/img/txt_btn_sb_02.png" alt="txt sm"></span></span>
        </button>
        <ins class="ripple"></ins>
      </div>
    </form>
    <!--/.frmComment-->
  </div>
</section>
<!--/.st_comment-->
<section class="st_answer">
  <div class="bkg_ttl">
    <div class="row">
      <h2>第1話への<strong>5594</strong>件のコメント中<span>1～100件目の表示</span></h2>
    </div>
  </div>
  <!--/.bkg_ttl-->
  <div class="gr_ans">
    <div class="row">
      <ul class="list_pag">
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
      </ul>
      <!--/.list_pag-->
      <div class="item_answer">
        <div class="top_ans">
          <figure>
            <img src="./assets/img/avartar_user.jpg" alt="Avartar User">
            <figcaption>山田太郎</figcaption>
          </figure>
        </div>
        <!--/.top_ans-->
        <dl>
          <dt>アリアドネ オフィシャルメンバーズに賭ける、<br>あなたのありったけの想いを書いて下さい。</dt>
          <dd>
            回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答
          </dd>
        </dl>
      </div>
      <!--/.item_answer-->
      <div class="item_answer">
        <div class="top_ans">
          <figure>
            <img src="./assets/img/avartar_user.jpg" alt="Avartar User">
            <figcaption>山田太郎</figcaption>
          </figure>
        </div>
        <!--/.top_ans-->
        <dl>
          <dt>アリアドネ オフィシャルメンバーズに賭ける、<br>あなたのありったけの想いを書いて下さい。</dt>
          <dd>
            回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答回答
          </dd>
        </dl>
      </div>
      <!--/.item_answer-->
    </div>
  </div>
  <!--/.gr_ans-->
</section>
<!--/.st_answer-->